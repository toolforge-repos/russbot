# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Russell Blau
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import locale
import MySQLdb
import pywikibot

import flask
from markupsafe import Markup

app = flask.Flask(__name__)

locale.setlocale(locale.LC_ALL, "")

@app.route('/')
@app.route('/index.html')
def index():
    return app.send_static_file("index.html")

def clean(title):
    if not isinstance(title, str):
        title = title.decode("utf-8")
    return title.replace("_", " ")

def convert_time(delta, use_seconds=False):
    # delta must be a timedelta instance
    hours = delta.seconds // 3600 # doesn't include days, so <= 23
    seconds = delta.seconds - 3600 * hours
    minutes = seconds // 60
    seconds = seconds - 60 * minutes

    if use_seconds:
        seconds += 1 * (delta.microseconds >= 500000)
    else:
        minutes += 1 * (seconds >= 30)
    result = []

    if delta.days:
        result.append("%i day%s" % (delta.days,
                                    "s" if delta.days != 1 else ""))
    if delta.days or hours:
        result.append("%i hour%s" % (hours,
                                     "s" if hours != 1 else ""))
    if delta.days or hours or minutes or not use_seconds:
        result.append("%i minute%s" % (minutes,
                                       "s" if minutes != 1 else ""))
    if use_seconds and (seconds
                        or (not delta.days and not hours and not minutes)):
        result.append("%i second%s" % (seconds, "s" if seconds != 1 else ""))

    return ", ".join(result)

def nav_line(limit, num, offset, pagename, **extras):
    # limit: max number of items to display
    # num: actual number of items retrieved from database
    # offset: offset from zero'th item
    # pagename: name of this page in URL format
    # extras: other parameters to attach to the URL
    suffix = ""
    for param in extras:
        suffix += f"&{param}={extras[param]}"
    if offset > 0:
        prior = offset - limit if offset > limit else 0
        result = f'<p>View (<a href="{pagename}?limit={limit}&offset={prior}{suffix}">previous {limit}</a>)'
    else:
        result = f'<p>View (previous {limit})'
    if num == limit:
        nexto = offset + limit
        result += f' (<a href="{pagename}?limit={limit}&offset={nexto}">next {limit}{suffix}</a>) '
    else:
        result += f' (next {limit}) '
    result += f'(<a href="{pagename}?limit=20&offset={offset}{suffix}">20</a> '
    result += f'| <a href="{pagename}?limit=50&offset={offset}{suffix}">50</a> '
    result += f'| <a href="{pagename}?limit=100&offset={offset}{suffix}">100</a> '
    result += f'| <a href="{pagename}?limit=250&offset={offset}{suffix}">250</a> '
    result += f'| <a href="{pagename}?limit=500&offset={offset}{suffix}">500</a>)</p>'
    return Markup(result)

@app.route('/csd')
def csd():
    try:
        s = pywikibot.Site()
        s.login()
        now = datetime.datetime.utcnow()
        as_of = now.strftime("%B %d, %Y at %H:%M (UTC)")

        reasons = {
            "Db-a1": "<span style='display:none'>A01</span>No context (A1)",
            "Db-a2": "<span style='display:none'>A02</span>Foreign copy (A2)",
            "Db-a3": "<span style='display:none'>A03</span>No content (A3)",
            "Db-a5": "<span style='display:none'>A05</span>Transwikied (A5)",
            "Db-a7": "<span style='display:none'>A07</span>Notability (A7)",
            "Db-a9": "<span style='display:none'>A09</span>Notability/music (A9)",
            "Db-a10": "<span style='display:none'>A10</span>Content fork (A10)",
            "Db-a11": "<span style='display:none'>A11</span>Obviously invented (A11)",
            "Db-g1": "<span style='display:none'>G01</span>Nonsense (G1)",
            "Db-g2": "<span style='display:none'>G02</span>Test page (G2)",
            "Db-g3": "<span style='display:none'>G03</span>Vandalism (G3)",
            "Db-g4": "<span style='display:none'>G04</span>Re-created (G4)",
            "Db-g5": "<span style='display:none'>G05</span>Banned user (G5)",
            "Db-g6": "<span style='display:none'>G06</span>Housekeeping (G6)",
            "Db-xfd": "<span style='display:none'>G06</span>Completed XfD (G6)",
            "Db-g7": "<span style='display:none'>G07</span>Author request (G7)",
            "Db-g8": "<span style='display:none'>G08</span>Orphaned dependent (G8)",
            "Db-g10": "<span style='display:none'>G10</span>Attack (G10)",
            "Db-g11": "<span style='display:none'>G11</span>Advertising (G11)",
            "Db-g12": "<span style='display:none'>G12</span>Copyvio (G12)",
            "Db-g13": "<span style='display:none'>G13</span>Abandoned draft/AFC (G13)",
            "Db-g14": "<span style='display:none'>G14</span>Unneeded disambiguation page (G14)",
            "Db-decline": "<span style='display:none'>GX</span>AfC that meets general CSD (G?)",
            "Db-redirnone": "<span style='display:none'>G08</span>Broken redirect (G8)",
            "Db-r2": "<span style='display:none'>R2</span>Cross-ns redirect (R2)",
            "Db-r3": "<span style='display:none'>R3</span>Implausible redirect (R3)",
            "Db-r4": "<span style='display:none'>R4</span>File redirect hiding Commons page (R4)",
            "Db-f1": "<span style='display:none'>F01</span>Redundant image (F1)",
            "Db-f2": "<span style='display:none'>F02</span>Corrupt/empty image (F2)",
            "Db-f3": "<span style='display:none'>F03</span>Restricted license (F3)",
            "Db-f7": "<span style='display:none'>F07</span>Invalid fair use claim (F7)",
            "Db-f8": "<span style='display:none'>F08</span>Same file on Commons (F8)",
            "Db-f9": "<span style='display:none'>F09</span>File copyvio (F9)",
            "Db-f10": "<span style='display:none'>F10</span>Bad file type (F10)",
            "Db-c1": "<span style='display:none'>C1</span>Empty category (C1)",
            "Db-u1": "<span style='display:none'>U1</span>User-requested (U1)",
            "Db-u2": "<span style='display:none'>U2</span>Non-existent user (U2)",
            "Db-u5": "<span style='display:none'>U5</span>Webhosting in userspace (U5)",
            "Deprecated": "<span style='display:none'>G06</span>Deprecated template (G6)",
            "Db-p1": "<span style='display:none'>P1</span>Portal that meets article CSD (P1)",
            "Db-p2": "<span style='display:none'>P2</span>Empty portal (P2)",
        }
        reasons["Db-fpcfail"] = reasons["Db-f2"]
        reasons["Db-imagepage"] = reasons["Db-g8"]
        reasons["Db-negublp"] = reasons["Db-g10"]
        reasons["Db-spamuser"] = reasons["Db-g11"]

        dbtemplates = set(
           ("Db", "Db-a1", "Db-a2", "Db-a3", "Db-a5", "Db-a7", "Db-a9", "Db-a10", "Db-a11",
            "Db-c1", "Db-c2", 
            "Db-f1", "Db-f2", "Db-f3", "Db-f5", "Db-f7", "Db-f9", "Db-f10", "Db-fpcfail",
            "Db-g1", "Db-g2", "Db-g3", "Db-g4", "Db-g5", "Db-g6", "Db-g7", 
            "Db-g8", "Db-g10", "Db-g11", "Db-g12", "Db-g13", "Db-g14",
            "Db-imagepage", "Db-spamuser", "Db-negublp",
            "Db-p1", "Db-p2", "Db-xfd", "Afc cleared",
            "Db-r2", "Db-r3", "Db-r4",
            "Db-t2", "Db-t3", 
            "Db-u1", "Db-u2", "Db-u5",
           )
        )
        csd_count = 0
        candidates = {}
        req = pywikibot.data.api.Request(
            site = s,
            action = "query",
            generator = "categorymembers",
            gcmtitle = "Category:Candidates for speedy deletion",
            gcmlimit = "max",
            indexpageids = "",
            prop = "info|templates",
            tllimit = "max",
            rawcontinue = ""
        )
        errormsgs = []
        tablerows = []
        while True:
            response = req.submit()
            if "query" not in response or "pageids" not in response["query"]:
                break
            csd_count += len(response["query"]["pageids"])
            for pid in response["query"]["pageids"]:
                pagedata = response["query"]["pages"][pid]
                if "templates" not in pagedata:
                    continue
                templates = set()
                for t in pagedata["templates"]:
                    colon = t["title"].find(":")
                    ttitle = t["title"][colon+1:] # strip off "Template:" prefix
                    if ttitle in dbtemplates:
                        templates.add(ttitle)
                if not templates:
                    errormsgs.append("Unrecognized template in [[%s]]" % pagedata["title"])
                    continue
                age = now - pywikibot.Timestamp.fromISOformat(pagedata["touched"])
                page = pywikibot.Page(s, pagedata["title"])
                if page not in candidates:
                    candidates[page] = {'page': page, 'age': age}
                templates.discard("Db")
                templates.discard("Afc cleared")
                if templates:
                    candidates[page]['reasons'] = templates
            if "query-continue" not in response or "categorymembers" not in response["query-continue"]:
                break
            req.update(response["query-continue"]["categorymembers"])

        c_sorted = sorted(list(candidates.values()), key=lambda i:i['age'],
                          reverse=True)
        for item in c_sorted:
            page = item['page']
            r_list = []
            contested = False
            try:
                rsns = item['reasons']
            except KeyError:
                rsns = []
            for r in rsns:
                if r == "Db-g8" and ("Db-r1" in rsns or "Db-templatecat" in rsns):
                    continue
                if r == "Db-g6" and "Deprecated" in rsns:
                    continue
                r_list.append(Markup(reasons[r]))
            if page.toggleTalkPage().exists():
                contested = True
            datarow = {
                'pageurl': page.title(as_url=True),
                'title': page.title(), 'timestamp':str(now - item['age']), 
                'age': convert_time(item['age']), 
                'contested': contested, 
                'reasons': r_list if r_list else "Unspecified"
            }
            tablerows.append(datarow)

        return flask.render_template("csd.html", datarows=tablerows, as_of=as_of, csd_count=csd_count)

    finally:
        pywikibot.stopme()

@app.route('/longdisambigs')
def longdisambigs():
    try:
        s = pywikibot.Site()
        s.login()
        limit = flask.request.args.get("limit", 50, type=int)
        offset = flask.request.args.get("offset", 0, type=int)
        direction = flask.request.args.get("dir", "desc")
        db = MySQLdb.connect(db='enwiki_p',
                             host="enwiki.web.db.svc.wikimedia.cloud",
                             read_default_file="/data/project/russbot/.my.cnf")
        cursor = db.cursor()
        # compute replag
        now = datetime.datetime.utcnow()
        cursor.execute("""
            SELECT rc_timestamp
            FROM recentchanges
            ORDER BY rc_timestamp DESC
            LIMIT 1
        """)
        ts = cursor.fetchone()[0]
        dt = datetime.datetime.strptime(ts.decode("utf-8"), "%Y%m%d%H%M%S")
        replag = convert_time(now - dt, True)
        as_of = dt.strftime("%B %d, %Y at %H:%M (UTC)")
        pagelist = []
        count = offset + 1
        db_error = False
        try:
            r = cursor.execute(f'''\
                  SELECT /* LIMIT:60 */ page_title, page_len FROM page
                    JOIN categorylinks on cl_from=page_id
                   WHERE cl_to="All_article_disambiguation_pages"
                ORDER BY page_len {direction}
                   LIMIT {offset}, {limit};'''
            )
            for row in cursor.fetchall():
                title = clean(row[0])
                countstr = str(count)
                countstr = Markup("&nbsp;" * (5 - len(countstr)) + countstr)
                pagelist.append({
                    'count': countstr,
                    'urltitle': title.replace(' ', '_'), 
                    'title': title, 
                    'length': f"{int(row[1]):,}"
                })
                count += 1
        except MySQLdb.OperationalError:
            db_error = True
            r = 0
            db.close()
        return flask.render_template("longdabs.html",
            as_of=as_of,
            replag=replag,
            limit=limit,
            offset=offset,
            dir=direction,
            db_error=db_error,
            num=r,
            pagelist=pagelist
        )
    finally:
        pywikibot.stopme()

@app.route("/commonbacklinks")
def common_backlinks():
    from flask import g
    try:
        s = pywikibot.Site()
        s.login()
        g.title = flask.request.args.get("title", "")
        g.invalid = False
        g.nopage = False
        g.results = []
        if g.title:
            try:
                p = pywikibot.Page(s, g.title)
                if not p.exists():
                    g.nopage = True
                else:
                    linktotals = {}
                    # get all articles that link to page p
                    for bl in p.backlinks(filter_redirects=False, namespaces=0):
                        # now count all the article links on each page
                        for fl in bl.linkedPages(namespaces=0, follow_redirects=True):
                            if fl != p:
                                linktotals[fl] = linktotals.get(fl, 0) + 1
                    # sort the results first by link count (decreasing), then by title (increasing)
                    g.results = sorted(linktotals.items(), key=lambda x: (-x[1], x[0]))
                    if len(g.results) > 25:
                        g.results = g.results[:25]
            except pywikibot.exceptions.InvalidTitleError:
                g.invalid = True
        return flask.render_template("commonback.html")
    finally:
        pywikibot.stopme()

@app.route("/cats_in_backlinks")
def cats_in_backlinks():
    from flask import g
    try:
        s = pywikibot.Site()
        s.login()
        g.title = flask.request.args.get("title", "")
        g.invalid = False
        g.nopage = False
        g.results = []
        if g.title:
            try:
                p = pywikibot.Page(s, g.title)
                if not p.exists():
                    g.nopage = True
                else:
                    linktotals = {}
                    # get all articles that link to page p
                    for bl in p.backlinks(filter_redirects=False, namespaces=0):
                        # now count all the category links on each page
                        for cat in bl.categories():
                            linktotals[cat] = linktotals.get(cat, 0) + 1
                    # sort the results first by link count (decreasing), then by title (increasing)
                    g.results = sorted(linktotals.items(), key=lambda x: (-x[1], x[0]))
                    if len(g.results) > 25:
                        g.results = g.results[:25]
            except pywikibot.exceptions.InvalidTitleError:
                g.invalid = True
        return flask.render_template("catsback.html")
    finally:
        pywikibot.stopme()
