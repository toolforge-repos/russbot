#!/usr/bin/python

import pywikibot
from pywikibot.pagegenerators import PreloadingGenerator as Preload
import urllib
from BeautifulSoup import BeautifulSoup as Soup
import time

#time.sleep(1800)

try:
    offset = 0
    limit = 500
    for arg in pywikibot.handle_args():
        if arg.startswith("-offset:"):
            offset = int(arg[len("-offset:"):])
        elif arg.startswith("-limit:"):
            limit = int(arg[len("-limit:"):])
    s = pywikibot.Site()
    s.login()
    s.throttle.setDelays(writedelay=1)
    source = \
"http://tools.wmflabs.org/dplbot/disambig_links.php?offset=%(offset)s&limit=%(limit)s" \
            % locals()
    page = urllib.urlopen(source)
    html = Soup(page)
    for link in html.findAll("tr"):
        anchor = link.find("td").find("a").text
        dab = pywikibot.Page(s, anchor)
        try:
            for ref in Preload(dab.backlinks(followRedirects=True, namespaces=0)):
                try:
                    text = ref.get(get_redirect = True)
                    ref.save("Pywikibot touch script")
                except pywikibot.NoPage:
                    pywikibot.error(u"Page %s does not exist."
                                    % ref.title(asLink=True))
                except pywikibot.IsRedirectPage:
                    pywikibot.warning(u"Page %s is a redirect; skipping."
                                      % ref.title(asLink=True))
                except pywikibot.LockedPage:
                    pywikibot.error(u"Page %s is locked."
                                    % ref.title(asLink=True))
                except pywikibot.PageNotSaved:
                    pywikibot.error(u"Page %s not saved."
                                    % ref.title(asLink=True))
                except UnicodeDecodeError:
                    pass
        except pywikibot.CircularRedirect:
            pass
finally:
    pywikibot.stopme()
