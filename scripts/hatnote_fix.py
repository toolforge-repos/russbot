#!/usr/bin/python3
# -*- coding: utf-8  -*-

"""
Edit intentional disambiguation links in hatnote templates.
"""

import pywikibot
from pywikibot import pagegenerators
import re, sys

hatnote_args = {
    'About': ['3', '5', '7', '9'],
#    'Distinguish': ['1', '2', '3', '4'],
    'For': ['2', '3', '4', '5'],
    'Other hurricanes': ['1'],
    'Other people': ['2'],
    'Other people2': ['1'],
    'Other people3': ['3'],
    'Other places': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
    'Other uses': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
    'Other uses of': ['2'],
    'Redirect': ['3', '5', '7', '9'],
    'Redirect2': ['4', '6', '8'],
    'Redirect6': ['3', '5'],
    'Redirect7': ['3', '5'],
#    'Redirect-distinguish': ['2', '3', '4', '5'],
#    'See also': ['1', '2', '3', '4', '5', '6', '7', '8', '9',
#                 '10', '11', '12', '13', '14', '15'],
}

def init_cap(title):
    """Return title with the first character capitalized if it is a letter."""
    return title[:1].upper() + title[1:]

def main():
    pywikibot.handle_args()
    pywikibot.config.put_throttle = 10
    s = pywikibot.Site()
    s.login()
    log = []
    # set up hatnote redirect lists
    hatnote_redirs = dict((h, [h]) for h in hatnote_args)
    for hat in hatnote_redirs:
        hatpage = pywikibot.Page(s, "Template:" + hat)
        hatnote_redirs[hat].extend(r.title(with_ns=False)
                                   for r in hatpage.backlinks(
                                       filter_redirects=True, namespaces=10))

    pagecount = 0
    editcount = 0
    redirectcount = 0
    # Generate all articles that contain dablinks
    dablink = pywikibot.Page(s, "Template:Hatnote")
    try:
        for templt in hatnote_args:
            gen = pywikibot.Page(s, "Template:%s" % templt).embeddedin(
                    filter_redirects=False, namespaces=0,
                    content=True)
            for article in gen:
                pagecount += 1
                changed = False
                templates = pywikibot.textlib.extract_templates_and_params(article.text)
                for (name, params) in templates:
                    for hat in hatnote_redirs:
                        if init_cap(name) in hatnote_redirs[hat]:
                            # this template inclusion is a recognized hatnote
                            break
                    else:
                        continue
                    # now check to see if the targeted params exist
                    for param_index in hatnote_args[hat]:
                        if param_index not in params:
                            continue
                        # p is title of an identified link
                        p = params[param_index].strip()
                        p = p.replace('\u00a0', '')
                        if "{" in p or not p:
                            continue
                        if p.endswith(")") or ")#" in p:
                            continue
                        try:
                            if s.isInterwikiLink(p):
                                continue
                        except pywikibot.exceptions.NoUsernameError:
                            continue
                        page = pywikibot.Page(s, p)
                        try:
                            # skip non-articles
                            if page.namespace() != 0:
                                continue
                            # skip redlinks
                            if not page.exists():
                                continue
                        except (AttributeError, pywikibot.exceptions.Error): # invalid link
                            if not p.startswith("#"):
                                log.append(
    "# [[%s]]: Unable to parse argument (not confirmed dab) %s=%s in {{tl|%s}}"
                                      % (article.title(), param_index, p, name))
                            continue
                        # follow redirects
                        if page.isRedirectPage():
                            try:
                                target = page.getRedirectTarget()
                            except pywikibot.exceptions.CircularRedirectError:
                                continue
                            if not target.exists():
                                continue
                            if target.namespace() != 0:
                                continue
                        else:
                            target = page
                        # skip non-disambig targets
                        if not target.isDisambig():
                            continue
                        if '#' in p:
                            basetitle, section = p.split("#", 1)
                            basetitle = basetitle.strip() + " (disambiguation)"
                            fulltitle = basetitle.strip() + "#" + section.strip()
                            disambig = pywikibot.Page(s, fulltitle)
                        else:
                            disambig = pywikibot.Page(s, p + " (disambiguation)")
                        try:
                            if not disambig.exists():
                                if "(disambiguation) (disambiguation)" in disambig.title():
                                    log.append(
        "# ERROR: tried to create redirect [[%s]]" % disambig.title())
                                    continue
            ##                    pageresult.append(u"#: Create redirect [[%s]] (for hatnote {{tlx|%s}}"
            ##                                 % (disambig.title(), name))
                                disambig.text = \
                                    "#REDIRECT [[%s]]{{R to disambiguation page}}" \
                                                  % target.title()
                                try:
                                    disambig.save(
        "Robot: Creating redirect to disambiguation page per [[WP:INTDABLINK]]",
                                        minor=False, watch="nochange")
                                except pywikibot.exceptions.Error:
                                    log.append(
        "# Unable to create redirect page [[%s]] to [[%s]]"
                                        % (disambig.title(), target.title()))
                                    continue
                                redirectcount += 1
                            # skip if page exists but is not a redirect
                            elif not disambig.isRedirectPage():
                                continue
                            # skip if redirect target is different than current link target
                            elif disambig.getRedirectTarget() != target:
                                continue
                        except pywikibot.exceptions.Server504Error:
                            continue
                        # edit the link
        ##                pageresult.append(u"#: Edit hatnote {{tlx|%s}}:" % name)
                        param_num = int(param_index)
                        m = re.search(
                            r"{{( *[%s]%s *)(?:\|[^{|}]*){%i}\|( *%s *)(?:\|[^{|}]*)*}}"
                              % (name[0].upper()+name[0].lower(),
                                 name[1:], param_num - 1, p),
                            article.text)
                        if not m:
                            if "(disambiguation)" not in p:
                                log.append(
                                    "# '''[[%s]]: Unable to parse argument %i=%s in {{tl|%s}}'''"
                                      % (article.title(), param_num, p, name))
                            continue
                        changed = True
                        tstart = m.start(1)
                        tend = m.end(1)
                        argstart = m.start(2)
                        argend = m.end(2)

                        copy = article.text[:]
                        if hat == "See also": #special case
                            copy = (copy[:argend+1]
                                    + "|l%s=%s" % (param_num, disambig.title())
                                    + copy[argend+1:])
                        else:
                            copy = (copy[:argstart]
                                    + disambig.title() + "{{!}}"
                                    + target.title()
                                    + copy[argend:])
                        copy = copy[:tstart] + hat + copy[tend:]
        ##                pageresult.append(
        ##                    u"#:<pre>- %s</pre>"
        ##                      % article.text[m.start(0):m.end(0)])
        ##                pageresult.append(
        ##                    u"#:<pre>+ %s</pre>"
        ##                      % copy[m.start(0):copy.find("}}", m.start(0))+2])
                        article.text = copy
                if changed:
                    try:
                        article.save(
    "Robot: Editing intentional link to disambiguation page in hatnote "
    "per [[WP:INTDABLINK]] [[User:RussBot#About the hatnote task|(explanation)]]",
                            )
                        editcount += 1
                    except pywikibot.exceptions.Error:
                        log.append("# Unable to edit [[%s]]" % article.title())
    except Exception as ex:
        pywikibot.exception(ex, tb=True)
    finally:
        if pagecount:
            log.insert(0,
    """Scanned %(pagecount)i pages, edited %(editcount)i pages, \
    created %(redirectcount)i new redirects.\n"""
                       % locals())
            logpage = pywikibot.Page(s, "User:RussBot/Hatnote bot log")
            logpage.text = "\n".join(log)
            try:
                logpage.save("creating log", minor=False)
            except:
                pass
        try:
            print(logpage.text)
        except:
            pass
        if 'request' in gen and 'params' in gen.request:
            for k in gen.request.params:
                if "continue" in k:
                    print(k, ":", gen.request.params[k])

if __name__ == "__main__":
    try:
        main()
    except Exception:
        pywikibot.exceptions.Error("Fatal error:", exc_info=True)
    finally:
        pywikibot.stopme()
