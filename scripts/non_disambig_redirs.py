#! /usr/bin/python3
"""Redirects with 'disambiguation' in the title, to non-disambiguation pages."""

import datetime
import MySQLdb
import pywikibot
import locale
import re
from reporttools import clean, quote, write

locale.setlocale(locale.LC_ALL, "")


try:
    disambig = " (disambiguation)"
    pywikibot.handle_args()
    s = pywikibot.Site()
    s.login()
    # preload set of Redirects with history
    rwh = set(pywikibot.Category(s, 
               "Category:Redirects with history").articles())
    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.analytics.db.svc.wikimedia.cloud",
                         read_default_file="/data/project/russbot/.my.cnf")
    cursor = db.cursor()
    offset = 0
    bdr = []

    while True:
        result = cursor.execute("""\
            SELECT source.page_title, rd_title /* SLOW_OK */
            FROM page AS source
                JOIN redirect ON source.page_id=rd_from
                JOIN page AS target ON rd_namespace=target.page_namespace
                                    AND rd_title=target.page_title
            WHERE source.page_namespace=0
                AND source.page_title LIKE '%%_(disambiguation)'
                AND rd_namespace=0
                AND NOT EXISTS (
                    SELECT *
                    FROM categorylinks
                    WHERE cl_from=target.page_id
                      AND cl_to IN ('All_article_disambiguation_pages',
                          'All_set_index_articles')
                )
            LIMIT %(offset)i, 500;
        """ % locals())
        if not result:
            break
        pywikibot.output("Processing %i results." % result)
        offset += result
        for row in cursor.fetchall():
            pg = pywikibot.Page(s, clean(row[0]))
            if pg in rwh:
                continue
            clnrow = clean(row[0]), clean(row[1])
            if clnrow[1].startswith("List of ") or clnrow[1].startswith("Lists of "):
                continue
            if clnrow[1] + disambig == clnrow[0]:
                line = "| {{noredirect|%s}} || - || [[%s]]" % clnrow
            else:
                dab2 = pywikibot.Page(s, clnrow[1] + disambig)
                if dab2.exists():
                    line = "| {{noredirect|%s}} || {{noredirect|%s}} || [[%s]] ([[%s|dab]])" \
                       % (clnrow[0], clnrow[0][:-len(disambig)], clnrow[1], dab2.title())                    
                else:
                    line = "| {{noredirect|%s}} || {{noredirect|%s}} || [[%s]]" \
                       % (clnrow[0], clnrow[0][:-len(disambig)], clnrow[1])
            # next line can throw CircularRedirect
            try:
                bl = list(pg.backlinks())
            except pywikibot.exceptions.CircularRedirectError:
                print(f"Circular redirect at [[{{pg.title()}}]]")
                continue
            blcount = sum(1 for i in bl if ( 
                             not i.title().startswith(
                             "User:RussBot/Non-disambiguation")) and "Links to (disambiguation) pages" not in i.title())
            blarts = sum(1 for i in bl if i.namespace() == 0)
            line = line + "|| %i || %i" % (blcount, blarts)
            bdr.append(line)

    cursor.close()
    db.close()

    write(s, "User:RussBot/Non-disambiguation redirects",
             'Redirects that have "disambiguation" in the title but redirect'
             ' to non-disambig articles:',
             sorted(bdr),
             "Bot: creating list from database mirror",
             chunksize=200,
             before='''\
{| class="wikitable"
! scope="col" | Redirect
! scope="col" | Original target
! scope="col" | Current target
! scope="col" | Incoming links
! scope="col" | ... from articles
|-
''',
             after='\n|}',
             joiner="\n|-\n"
          )

except Exception:
    pywikibot.error("Error", exc_info=True)
finally:
    pywikibot.stopme()
