#! /usr/bin/python3
"""Malplaced disambiguation pages."""

import datetime
import MySQLdb
import pywikibot
import locale
import re
from reporttools import clean, quote, write
from refcount import mainrefs

locale.setlocale(locale.LC_ALL, "")

try:
    s = pywikibot.Site()
    s.login()

    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.analytics.db.svc.wikimedia.cloud",
                         read_default_file="/data/project/russbot/.my.cnf")

    cursor = db.cursor()
    offset = 0
    mdp = []

#   1.  Traditional malplaced disambiguation pages
    while True:
        result = cursor.execute(
                """
                SELECT page_title, rd_title /* SLOW_OK */
                FROM page JOIN redirect ON page_id=rd_from
                WHERE page_namespace=0 and rd_namespace=0
                      and (rd_title = CONCAT(page_title, "_(disambiguation)")
                        or rd_title = CONCAT(page_title, "_(Disambiguation)"))
                LIMIT %(offset)i, 5000;
                """ % locals()
                )
        if not result:
            break
        print("Processing %i results." % result)
        offset += result
        for row in cursor.fetchall():
            clnrow = clean(row[0]), clean(row[1])
            mdp.append("# {{noredirect|%s}} redirects to [[%s]]" % clnrow)

    # 2.  Duplicate disambiguation pages
    dups = []
    offset = 0
    while True:
        result = cursor.execute(
             """SELECT page1.page_title, page2.page_title
                /* SLOW_OK */
                FROM page AS page1
                  JOIN categorylinks AS cl1 ON cl1.cl_from=page1.page_id
                  JOIN page AS page2
                    ON page2.page_title = CONCAT(page1.page_title, "_(disambiguation)")
                  JOIN categorylinks AS cl2 ON cl2.cl_from=page2.page_id
                WHERE page1.page_namespace=0 AND page2.page_namespace=0
                  AND NOT page1.page_is_redirect
                  AND NOT page2.page_is_redirect
                  AND cl1.cl_to = "All_article_disambiguation_pages"
                  AND cl2.cl_to = "All_article_disambiguation_pages"
                LIMIT %(offset)i, 5000;
             """ % locals())
        if not result: break
        for row in cursor.fetchall():
             dups.append("# [[%s]], [[%s]]"
                 % (clean(row[0]), clean(row[1])))
        offset += result
        print(offset)

    # 3. Foo (disambiguation) exists but Foo is a redlink
    dabtitles = []
    missing_nolinks = []
    missing_withlinks = []
    offset = 0
    while True:
        result = cursor.execute("""
            SELECT dab.page_title FROM page AS dab /* SLOW_OK */
            WHERE dab.page_namespace=0
            AND (NOT dab.page_is_redirect)
            AND dab.page_title LIKE '%%!_(disambiguation)' ESCAPE '!'
            LIMIT %(offset)i, 5000;
        """ % locals())
        if not result: break
        for row in cursor.fetchall():
            dabtitles.append(clean(row[0]))
        offset += result
        print(offset)
    
    while dabtitles:
        pool = dabtitles[:50]
        dabtitles = dabtitles[50:]
        primaries = [t[:-17] for t in pool] # 17 == len(" (disambiguation)")
        r = pywikibot.data.api.Request(site=s, parameters={
            'action': "query",
            'prop': "info",
            'indexpageids': "",
            'titles': primaries
        })
        data = r.submit()
        if "query" not in data or "pageids" not in data["query"] \
                or "pages" not in data["query"]:
            print(list(data.keys()))
            break
        for p in data["query"]["pageids"]:
            pagedata = data["query"]["pages"][p]
            if "missing" in pagedata:
                thispage = pywikibot.Page(s, pagedata["title"])
                backlinks = len(list(mainrefs(thispage)))
                if backlinks > 0:
                    missing_withlinks.append(
"# [[%s (disambiguation)]], [[%s]] (%s [{{fullurl:Special:WhatLinksHere|target=%s&namespace=0}} links])"
                        % (pagedata["title"], pagedata["title"],
                           backlinks, pagedata["title"].replace(" ", "_")))
                else:
                    missing_nolinks.append(
                        "# [[%s (disambiguation)]], [[%s]]"
                        % (pagedata["title"], pagedata["title"]))
        
    miss_data_1 = "====No incoming links====\n\n" + \
                  "\n".join(sorted(missing_nolinks))
    miss_data_2 = "\n\n====Incoming links====\n\n" + \
                  "\n".join(sorted(missing_withlinks))
    miss_data = miss_data_1 + miss_data_2

    # get timestamp
    cursor.execute("""
        SELECT rc_timestamp
        FROM recentchanges
        ORDER BY rc_timestamp DESC
        LIMIT 1
    """)
    ts = cursor.fetchone()[0].decode()
    dt = datetime.datetime.strptime(ts, "%Y%m%d%H%M%S")
    timestamp = dt.strftime("%H:%M, %d %B %Y (UTC)")
    print(timestamp)
    
    cursor.close()
    db.close()

    report = pywikibot.Page(s,
            "Wikipedia:WikiProject Disambiguation/Malplaced disambiguation pages")

    begin = report.text.find("==Database report==")
    end = report.text.find("==Instructions==")
    mdp_data = "\n".join(sorted(mdp))
    dup_data = "\n".join(sorted(dups))

    report.text = (report.text[:begin] + """\
==Database report==
=== Malplaced disambiguation pages ===
As of %(timestamp)s, these pages redirect to their own title with \
"&nbsp;(disambiguation)" appended. '''Do not manually add pages to this \
list'''; use the [[#Manual list|Manual list]] above instead. Completed entries \
will be removed from the list, there is no need to strike them.

%(mdp_data)s

=== Duplicate disambiguation pages ===
As of %(timestamp)s, the following pairs of potentially duplicative \
disambiguation pages exist.

%(dup_data)s

=== Missing primary topics ===
As of %(timestamp)s, the following disambiguation pages exist without any \
page existing at the unqualified title. These pages are generally erroneous, \
and should be moved to the title without the parenthetical, leaving a \
redirect behind for intentional links to the disambiguation page. However, \
some of these pages should have a primary topic at the base title, or should \
have the primary topic redirect to an unambiguous topic. '''DO NOT''' move \
these pages if doing so will create disambiguation links; instead, fix the \
incoming links first, and then move the page.

%(miss_data)s

"""
        % locals() + report.text[end:])

    report.save(summary="Bot: automatically updating database report")

finally:
    pywikibot.stopme()
