#! /usr/bin/python3
# report erroneous and self-referential redirect hatnotes

import difflib
import itertools
import mwparserfromhell as mwp
import pywikibot
import re
from reporttools import *

def template_regex(t):
    '''Return a regex matching an invocation of template t, with params'''
    ttitle = t.title(with_ns=False)
    if ttitle[:1].isalpha():
        ttitle = "[%s%s]" % (ttitle[:1].lower(), ttitle[:1].upper()) + ttitle[1:]
    pattern = r'\{\{ *(?:[Tt]emplate:)?%s[^}|]*(\|[^}|]*)(\|[^}]*)?\}\}' \
              % ttitle
    return re.compile(pattern)

def title_case(title):
    '''Return a title with the first char capitalized, if it is a letter'''
    if title[:1].islower():
        return title[:1].upper() + title[1:]
    else:
        return title

try:
    pywikibot.handle_args()
    s = pywikibot.Site()
    errors = []
    log = []
    recursive = []
    count = 0
    redir = pywikibot.Page(s, "Template:Redirect")
    redirtemplates = set(r.title(with_ns=False) for r in
            itertools.chain([redir],
                redir.backlinks(follow_redirects=False, filter_redirects=True)))
    for article in s.page_embeddedin(redir, namespaces=0, content=True):
        # article contains a {{redirect}} hatnote
        hatnote_count = 0
        wikitext = mwp.parse(article.text)
        params = None
        for tnode in wikitext.filter_templates():
            if title_case(str(tnode.name).strip()) in redirtemplates:
                hatnote_count += 1
                if tnode.has(1):
                    redir = tnode.get(1).value
                else:
                    # redirect template with no parameters
                    errors.append("#[[%s]]: Redirect template without parameter" % article.title())
                    continue
                if redir.filter_templates():
                    try:
                        redir = s.expand_text(str(redir))
                    except:
                        pass
                if title_case(str(redir).strip()) == article.title():
                    recursive.append('#[[%s]]: Recursive redirect hatnote'
                           % article.title())
                else:
                    try:
                        pagetocheck = pywikibot.Page(s, redir)
                        if not pagetocheck.isRedirectPage() \
                               or pagetocheck.getRedirectTarget().title(with_section=False) != article.title():
                            log.append("#[[%s]]: [[%s]] is not a redirect to this page"
                                        % (article.title(), pagetocheck.title()))
                    except pywikibot.exceptions.Error:
                        errors.append('#[[%s]]: "%s" is not a valid page title'
                           % (article.title(), redir))
        if hatnote_count == 0:
            #            errors.append("#[[%s]]: No hatnote found" % article.title())
            continue
    write(s, "User:RussBot/Invalid redirect hatnotes",
          "Pages with apparently invalid {{tl|redirect}} hatnotes:",
          sorted(errors + log),
          "Robot: creating report",
          100)
    write(s, "User:RussBot/Recursive redirect hatnotes",
          "Pages with self-referential {{tl|redirect}} hatnotes:",
          sorted(recursive),
          "Robot: creating report",
          100)
except:
    pywikibot.logging.exception('')
finally:
    pywikibot.stopme()
