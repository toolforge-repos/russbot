#! /usr/bin/python
"""Disambiguation page redirects containing genre parentheticals."""

import datetime
import MySQLdb
import pywikibot
import re
from reporttools import clean, quote, write
from collections import defaultdict

def initcap(title):
    return title[:1].upper() + title[1:]

try:
    pywikibot.handleArgs()
    s = pywikibot.Site()
    s.login()
    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.analytics.db.svc.wikimedia.cloud",
                         read_default_file="/data/project/russbot/.my.cnf")
    cursor = db.cursor()
    offset = 0
    results = []

    while True:
        result = cursor.execute("""\
            SELECT r.page_title, p.page_title /*SLOW_OK*/
            FROM page as p,
                 p50380g50692__DPL_p.dab_link_count as count,
                 page as r,
                 redirect
            WHERE p.page_id = count.lc_id
             AND p.page_namespace = 0
             AND rd_namespace = 0
             AND rd_title = p.page_title
             AND r.page_id = rd_from
             AND (r.page_title LIKE '%%(song)%%'
                 OR r.page_title LIKE '%%(film)%%'
                 OR r.page_title LIKE '%%(album)%%'
                 OR r.page_title LIKE '%%(TV series)%%'
                 OR r.page_title LIKE '%%(video game)%%'
             )
            LIMIT %(offset)i, 5000;""" % locals())
        if not result:
            break
        offset += result
        for row in cursor.fetchall():
            results.append('# [[%s]] => [[%s]]' % (clean(row[0]), clean(row[1])))

    write(s, "User:RussBot/Genre redirects to disambiguation pages",
             'Redirects to disambiguation pages containing genre parentheticals:',
             sorted(results),
             "Bot: creating list from database mirror",
             chunksize=1000,
          )

except Exception:
    pywikibot.error("Error", exc_info=True)
finally:
    pywikibot.stopme()
