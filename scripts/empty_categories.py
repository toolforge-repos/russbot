#! /usr/bin/python

"""Empty categories report."""

# Copyright (c) 2013 russblau

# Portions subject to the following license:

# Copyright 2008 bjweeks, MZMcBride, CBM

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import MySQLdb
import pywikibot
import locale
import re
from reporttools import clean, quote, write

locale.setlocale(locale.LC_ALL, "")

try:
    s = pywikibot.Site()
    s.login()

    dbrpage = pywikibot.Page(s, "Wikipedia:Database reports/Empty categories")
    h = dbrpage.getVersionHistory(total=5)
    for item in h:
        timestamp = item[1]
        if (pywikibot.Timestamp.now() - timestamp) <= datetime.timedelta(2) \
               and item[2] == "BernsteinBot":
            raise RuntimeError("BernsteinBot has edited within past 48 hours.")
        
    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.labsdb",
                         read_default_file="/data/project/russbot/.my.cnf")

    cursor = db.cursor()
    output = []

    result = cursor.execute(
        """
/* emptycats.py SLOW_OK */
SELECT
  page_title,
  page_len
FROM categorylinks
RIGHT JOIN page ON cl_to = page_title
WHERE page_namespace = 14
AND page_is_redirect = 0
AND cl_to IS NULL
AND NOT EXISTS (SELECT
                  1
                FROM categorylinks
                WHERE cl_from = page_id
                AND (cl_to = 'Wikipedia_soft_redirected_categories'
                    OR cl_to = 'Disambiguation_categories'
                    OR cl_to LIKE 'Empty_categories%'))
AND NOT EXISTS (SELECT
                  1
                FROM templatelinks
                WHERE tl_from = page_id
                AND tl_namespace = 10
                AND tl_title = 'Empty_category');
        """ 
    )
    print "Processing %i results." % result
    i = 1
    for row in cursor.fetchall():
        if not re.search(r'(-importance|-class|non-article|assess|_articles_missing_|_articles_in_need_of_|_articles_undergoing_|_articles_to_be_|_articles_not_yet_|_articles_with_|_articles_without_|_articles_needing_|Wikipedia_featured_topics)', row[0], re.I|re.U):
           page_title = u'{{clh|1=%s}}' % unicode(row[0], 'utf-8')
           page_len = row[1]
           table_row = u'''| %d
| %s
| %s
|-''' % (i, page_title, page_len)
           output.append(table_row)
           i += 1

    cursor.execute('SELECT UNIX_TIMESTAMP() - UNIX_TIMESTAMP(rc_timestamp) FROM recentchanges ORDER BY rc_timestamp DESC LIMIT 1;')
    rep_lag = int(cursor.fetchone()[0])
    current_of = (datetime.datetime.utcnow() - datetime.timedelta(seconds=rep_lag)).strftime('%H:%M, %d %B %Y (UTC)')

    cursor.close()
    db.close()

    write(s, title = "Wikipedia:Database reports/Empty categories",
             header = '''Empty categories not in [[:Category:Wikipedia category redirects]], not in \
[[:Category:Disambiguation categories]], and do not contain "(-importance|\
-class|non-article|assess|articles missing|articles in need of|articles undergoing|\
articles to be|articles not yet|articles with|articles without|articles needing|\
Wikipedia featured topics)"; data as of <onlyinclude>%s</onlyinclude>.''' % current_of,
             text = output,
             chunksize = 5000,
             before = '''\
{| class="wikitable sortable plainlinks" style="width:100%; margin:auto;"
|- style="white-space:nowrap;"
! No.
! Category
! Length
|-
''',
             after = '\n|}\n',
             comment = "Updated list.",
             nopages = True
    )

except:
    pywikibot.logging.exception('')
finally:
    pywikibot.stopme()
