#!/usr/bin/python3
"""Redirects with 'disambiguation' in the title, but not marked as such."""

import datetime
import MySQLdb
import pywikibot
#import locale
import re
from reporttools import clean, quote, write

#locale.setlocale(locale.LC_ALL, "en_US")

try:
    EDITMAX = 100000
    pywikibot.handle_args()
    s = pywikibot.Site()
    s.login()

    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.labsdb",
                         read_default_file="/data/project/russbot/.my.cnf")

    cursor = db.cursor()
    offset = 0
    bdr = []
    report = []
    edit_count = 0

    while True:
        result = cursor.execute("""\
            SELECT source.page_title, rd_title /* SLOW_OK */
            FROM page AS source
                JOIN redirect ON source.page_id=rd_from
                JOIN page AS target ON rd_namespace=target.page_namespace
                                    AND rd_title=target.page_title
            WHERE source.page_namespace=0
                AND source.page_title LIKE '%%(disambiguation)'
                AND rd_namespace=0
                AND NOT EXISTS (
                    SELECT *
                    FROM templatelinks, linktarget
                    WHERE tl_from=source.page_id
                      AND tl_target_id = lt_id
                      AND lt_namespace=10
                      AND lt_title="R_to_disambiguation_page"
                )
                AND EXISTS (
                    SELECT *
                    FROM categorylinks
                    WHERE cl_from=target.page_id
                      AND cl_to="All_disambiguation_pages"
                )
            ORDER BY source.page_title
            LIMIT %(offset)i, 5000;
                 """ % locals()
        )
        if not result:
            break
        print("Processing", result, "results.")
        offset += result
        for row in cursor.fetchall():
            clnrow = clean(row[0]), clean(row[1])
            if clnrow[0] != clnrow[1] + " (disambiguation)":
                continue
            bdr.append(clnrow)

    cursor.close()
    db.close()

    cat = pywikibot.Category(s, "Category:Redirects to disambiguation pages")
    for data in bdr:
        if edit_count < EDITMAX:
            page_to_fix = pywikibot.Page(s, data[0])
            if cat in set(page_to_fix.categories()):
                report.append("# {{n.b.}} {{noredirect|%s}} already contains redirect template; not changed"
                              % data[0])
                continue
            try:
                fix_text = re.sub(r'(?i)\{+ *r +to +disambiguation +page *\}+',
                                  '{{R to disambiguation page}}',
                                  page_to_fix.text)
                fix_text = re.sub(r'(?i)\{+ *r +from +disambig *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +from +disambiguation *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +to +disambig *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +to +disambiguation *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *redirect +to +disambiguation +page *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +to +dab *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +disambig *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                fix_text = re.sub(r'(?i)\{+ *r +dab *\}+',
                                  '{{R to disambiguation page}}',
                                  fix_text)
                if fix_text != page_to_fix.text:
                    page_to_fix.text = fix_text
                    page_to_fix.save(summary="Bot: redirect template cleanup")
                    report.append(
                        "# {{n.b.}} {{noredirect|%s}} redirects to [[%s]]; hatnote cleaned"
                          % data)
                else:
                    # look for Redirect category shell
                    param = None
                    for (tpage, paramlist) in page_to_fix.templatesWithParams():
                        if tpage.isRedirectPage():
                            tpage = tpage.getRedirectTarget()
                        if tpage.title() == "Template:Redirect category shell":
                            param = paramlist[0]
                            break
                    if param is not None:
                        page_to_fix.text = page_to_fix.text.replace(
                            param,
                            param.rstrip() + "\n{{R to disambiguation page}}\n"
                        )
                    else:
                        page_to_fix.text = page_to_fix.text \
                                       + "{{R to disambiguation page}}"
                    page_to_fix.save(summary="Bot: adding redirect template")
                    report.append(
                        "# {{fixed}} {{noredirect|%s}} redirects to [[%s]]"
                          % data)
                edit_count += 1
            except pywikibot.Error:
                report.append(
                    "# {{cross}} {{noredirect|%s}} redirects to [[%s]]"
                      % data)
        else:
            report.append(
                "# {{noredirect|%s}} redirects to [[%s]]"
                  % data)
    print(len(report), "pages to report.")
    write(s, "User:RussBot/Redirects to disambiguation pages",
             'Redirects to be marked with {{tl|R to disambiguation page}}:',
             report,
             "Bot: creating list from database mirror")
finally:
    pywikibot.stopme()
