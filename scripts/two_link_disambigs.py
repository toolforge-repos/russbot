#! /usr/bin/python
"""Disambiguation pages containing only two links."""

import datetime
import MySQLdb
import pywikibot
import re
from reporttools import clean, quote, write
from collections import defaultdict

def initcap(title):
    return title[:1].upper() + title[1:]

try:
    pywikibot.handle_args()
    s = pywikibot.Site()
    s.login()
    db = MySQLdb.connect(db='enwiki_p',
                         host="enwiki.analytics.db.svc.wikimedia.cloud",
                         read_default_file="/data/project/russbot/.my.cnf")
    cursor = db.cursor()
    offset = 0
    linkcount = defaultdict(int)

    result = cursor.execute("""\
            SELECT page_title, pl_title /*SLOW_OK*/
            FROM page JOIN pagelinks, linktarget
            WHERE page_namespace = 0
             AND pl_from = page_id
             AND lt_id = pl_target_id
             AND lt_namespace = 0 
             AND (SELECT COUNT(*) FROM categorylinks
                  WHERE cl_from = page_id
                  AND cl_to = "All_article_disambiguation_pages") > 0
            """)
    for row in cursor.fetchall():
        linkcount[clean(row[0])] += 1

    twodabs = set()
    for title in linkcount:
        if linkcount[title] <= 2:
            twodabs.add(title)
    write(s, "User:RussBot/Two-link disambiguation pages",
             'Disambiguation pages that link to only two articles:',
             [("# [[%s]]%s" % (t, (" *" if linkcount[t]<2 else "")))
		     for t in sorted(twodabs)],
             "Bot: creating list from database mirror",
             chunksize=1000,
          )

except Exception:
    pywikibot.error("Error", exc_info=True)
finally:
    pywikibot.stopme()
