#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""This bot will move pages out of redirected categories

Usage: category_redirect.py [options]

The bot will look for categories that are marked with a category redirect
template, take the first parameter of the template as the target of the
redirect, and move all pages and subcategories of the category there. It
also changes hard redirects into soft redirects, and fixes double redirects.
A log is written under <userpage>/category_redirect_log. Only category pages
that haven't been edited for a certain cooldown period (currently 7 days)
are taken into account.

"""

#
# (C) Pywikibot team, 2008-2014
#
# Distributed under the terms of the MIT license.
#
__version__ = '$Id$'
#

import pickle
import re
import time
from datetime import datetime, timedelta, date
import pywikibot
from pywikibot import pagegenerators
from pywikibot import i18n


class CategoryRedirectBot:

    def __init__(self):
        self.cooldown = 7  # days
        self.cooldown_msg = None
        self.today = datetime.now()
        self.site = pywikibot.Site()
        self.site.login()
        self.catprefix = self.site.namespace(14) + ":"
        self.log_text = []
        self.edit_requests = []
        self.log_page = pywikibot.Page(self.site,
                                       "User:%(user)s/category redirect log"
                                       % {'user': self.site.user()})

        # HACK
        if self.site.family.name == "commons":
            self.site.family.category_redirect_templates['commons'] = \
                self.site.family.category_redirect_templates['_default']
            self.cooldown_msg = pywikibot.Page(self.site, "Template:CR_cooldown")
            self.cooldown_text = "{{" + self.cooldown_msg.title(with_ns=False) + "}}\n"

        # Localization:

        # Category that contains all redirected category pages
        self.cat_redirect_cat = {
            'wikipedia': {
                'ar': "تصنيف:تحويلات تصنيفات ويكيبيديا",
                'cs': "Kategorie:Zastaralé kategorie",
                'da': "Kategori:Omdirigeringskategorier",
                'en': "Category:Wikipedia soft redirected categories",
                'es': "Categoría:Wikipedia:Categorías redirigidas",
                'fa': "رده:رده‌های منتقل‌شده",
                'hu': "Kategória:Kategóriaátirányítások",
                'ja': "Category:移行中のカテゴリ",
                'no': "Kategori:Wikipedia omdirigertekategorier",
                'pl': "Kategoria:Przekierowania kategorii",
                'pt': "Categoria:!Redirecionamentos de categorias",
                'ru': "Категория:Википедия:Категории-дубликаты",
                'simple': "Category:Category redirects",
                'sh': "Kategorija:Preusmjerene kategorije Wikipedije",
                'vi': "Thể loại:Thể loại đổi hướng",
                'zh': "Category:已重定向的分类",
            },
            'commons': {
                'commons': "Category:Category redirects"
            }
        }

        self.move_comment = 'category_redirect-change-category'
        self.redir_comment = 'category_redirect-add-template'
        self.dbl_redir_comment = 'category_redirect-fix-double'
        self.maint_comment = 'category_redirect-comment'
        self.edit_request_text = i18n.twtranslate(
            self.site.code, 'category_redirect-edit-request') + '\n~~~~'
        self.edit_request_item = i18n.twtranslate(
            self.site.code, 'category_redirect-edit-request-item')

    def move_contents(self, oldCatTitle, newCatTitle, editSummary):
        """The worker function that moves pages out of oldCat into newCat"""
        LIMIT = 200
        while True:
            try:
                oldCat = pywikibot.Category(self.site,
                                            self.catprefix + oldCatTitle)
                newCat = pywikibot.Category(self.site,
                                            self.catprefix + newCatTitle)

                oldCatLink = oldCat.title()
                newCatLink = newCat.title()
                comment = editSummary % locals()
                # Move articles
                found, moved = 0, 0
                for article in oldCat.members():
                    found += 1
                    if found <= LIMIT:
                        changed = article.change_category(oldCat, newCat,
                                                          summary=comment)
                        if changed:
                            moved += 1
                        elif article.exists():
                            article.save()

                # pass 2: look for template doc pages
                for item in pywikibot.data.api.ListGenerator(
                        "categorymembers", cmtitle=oldCat.title(),
                        cmprop="title|sortkey", cmnamespace="10",
                        cmlimit="max", site=self.site):
                    if found >= LIMIT:
                        break
                    doc = pywikibot.Page(pywikibot.Link(item['title'] +
                                                        "/doc", self.site))
                    try:
                        doc.get()
                    except pywikibot.exceptions.Error:
                        continue
                    changed = doc.change_category(oldCat, newCat,
                                                  summary=comment)
                    if changed:
                        moved += 1

                if found:
                    pywikibot.output("%s: %s found, %s moved"
                                     % (oldCat.title(), found, moved))
                return (found, moved)
            except pywikibot.exceptions.ServerError:
                pywikibot.output("Server error: retrying in 5 seconds...")
                time.sleep(5)
                continue
            except KeyboardInterrupt:
                raise
            except:
                return (None, None)

    def readyToEdit(self, cat):
        """Return True if cat not edited during cooldown period, else False."""
        deadline = self.today + timedelta(days=-self.cooldown)
        if cat.latest_revision.timestamp is None:
            raise RuntimeError
        return deadline > cat.latest_revision.timestamp

    def get_log_text(self):
        """Rotate log text and return the most recent text."""
        LOG_SIZE = 7  # Number of items to keep in active log
        try:
            log_text = self.log_page.get()
        except pywikibot.exceptions.NoPageError:
            log_text = ""
        log_items = {}
        header = None
        for line in log_text.splitlines():
            if line.startswith("==") and line.endswith("=="):
                header = line[2:-2].strip()
            if header is not None:
                log_items.setdefault(header, [])
                log_items[header].append(line)
        if len(log_items) < LOG_SIZE:
            return log_text
        # sort by keys and keep the first (LOG_SIZE-1) values
        keep = [text for (key, text) in
                sorted(list(log_items.items()), reverse=True)[:LOG_SIZE - 1]]
        log_text = "\n".join("\n".join(line for line in text) for text in keep)
        # get permalink to older logs
        history = list(self.log_page.revisions(total=LOG_SIZE))
        # get the id of the newest log being archived
        rotate_revid = history[LOG_SIZE - 1]['revid']
        # append permalink
        log_text = log_text + ("\n\n'''[%s Older logs]'''"
                               % self.log_page.permalink(oldid=rotate_revid))
        return log_text

    def run(self):
        """Run the bot"""
        global destmap, catlist, catmap

        user = self.site.user()
        problems = []
        newredirs = []

        l = time.localtime()
        today = "%04d-%02d-%02d" % l[:3]
        edit_request_page = pywikibot.Page(
            self.site, "User:%(user)s/category edit requests" % locals())
        datafile = pywikibot.config.datafilepath("%s-catmovebot-data"
                                                 % self.site.dbName())
        try:
            inp = open(datafile, "rb")
            record = pickle.load(inp)
            inp.close()
        except (IOError, EOFError):
            record = {}
        if record:
            pickle.dump(record, open(datafile + ".bak", "wb"), -1)

        template_list = self.site.category_redirects()
        # regex to match soft category redirects
        #  note that any templates containing optional "category:" are
        #  incorrect and will be fixed by the bot
        template_regex = re.compile(
            r"""{{\s*(?:%(prefix)s\s*:\s*)?  # optional "template:"
                     (?:%(template)s)\s*\|   # catredir template name
                     (\s*%(catns)s\s*:\s*)?  # optional "category:"
                     ([^|}]+)                # redirect target cat
                     (?:\|[^|}]*)*}}         # optional arguments 2+, ignored
             """ % {'prefix': self.site.namespace(10).lower(),
                    'template': "|".join(item.replace(" ", "[ _]+")
                                         for item in template_list),
                    'catns': self.site.namespace(14)},
            re.I | re.X)

        # check for hard-redirected categories that are not already marked
        # with an appropriate template
        comment = i18n.twtranslate(self.site.code, self.redir_comment)
        for page in pagegenerators.PreloadingGenerator(
                self.site.allpages(namespace=14, filterredir=True), groupsize=250):
            if not page.botMayEdit():
                if date.today().weekday() == 0:
                    problems.append("* Not allowed to edit %s"
                            % page.title(as_link=True, textlink=True))
                continue
            # generator yields all hard redirect pages in namespace 14
            if self.site.family.name == "commons" and page.title().endswith("/Views"):
                continue
            if page.isCategoryRedirect():
                # this is already a soft-redirect, so skip it (for now)
                continue
            try:
                target = page.getRedirectTarget()
            except pywikibot.exceptions.InterwikiRedirectPageError:
                problems.append("# %s is an interwiki redirect"
                                % page.title(as_link=True, textlink=True))
                continue
            except pywikibot.exceptions.CircularRedirectError:
                target = page
                problems.append("# %s is a self-linked redirect"
                                % page.title(as_link=True, textlink=True))
            except (RuntimeError, UnicodeDecodeError):
                # race condition: someone else removed the redirect while we
                # were checking for it
                continue
            if target.namespace() == 14:
                # this is a hard-redirect to a category page
                newtext = ("{{%(template)s|%(cat)s}}"
                           % {'cat': target.title(with_ns=False),
                              'template': template_list[0]})
                try:
                    page.text = newtext
                    page.save(comment)
                    self.log_text.append("* Added {{tl|%s}} to %s"
                                         % (template_list[0],
                                            page.title(as_link=True,
                                                       textlink=True)))
                except pywikibot.exceptions.Error:
                    self.log_text.append("* Failed to add {{tl|%s}} to %s"
                                         % (template_list[0],
                                            page.title(as_link=True,
                                                       textlink=True)))
            else:
                problems.append("# %s is a hard redirect to %s"
                                % (page.title(as_link=True, textlink=True),
                                   target.title(as_link=True, textlink=True)))

        pywikibot.output("Done checking hard-redirect category pages.")

        comment = i18n.twtranslate(self.site.code, self.move_comment)
        counts, destmap, catmap = {}, {}, {}
        catlist, nonemptypages = [], []
        redircat = pywikibot.Category(
            pywikibot.Link(self.cat_redirect_cat
                           [self.site.family.name][self.site.code], self.site))

        # get a list of all members of the category-redirect category
        catpages = dict((c, None)
                        for c in redircat.subcategories())

        # check the category pages for redirected categories
        pywikibot.output("")
        pywikibot.output("Checking %s category redirect pages"
                         % len(catpages))
        for cat in catpages:
            cat_title = cat.title(with_ns=False)
            if "category redirect" in cat_title or cat_title == "Delete":
                self.log_text.append("* Ignoring %s"
                                     % cat.title(as_link=True, textlink=True))
                continue
            if hasattr(cat, "_catinfo"):
                # skip empty categories that don't return a "categoryinfo" key
                catdata = cat.categoryinfo
                if "size" in catdata and int(catdata['size']):
                    # save those categories that have contents
                    nonemptypages.append(cat)
            if cat_title not in record:
                # make sure every redirect has a record entry
                record[cat_title] = {today: None}
                try:
                    newredirs.append("*# %s -> %s"
                                     % (cat.title(as_link=True, textlink=True),
                                        cat.getCategoryRedirectTarget().title(
                                            as_link=True, textlink=True)))
                except pywikibot.exceptions.Error:
                    pass
                # do a null edit on cat
##                try:
##                    if cat.exists():
##                        cat.save()
##                except:
##                    pass

        # delete record entries for non-existent categories
        for cat_name in list(record.keys()):
            if pywikibot.Category(self.site,
                                  self.catprefix + cat_name) not in catpages:
                del record[cat_name]

        pywikibot.output("")
        pywikibot.output("Moving pages out of %s redirected categories."
                         % len(nonemptypages))

        for cat in pagegenerators.PreloadingGenerator(nonemptypages):
            try:
                if not cat.isCategoryRedirect():
                    self.log_text.append("* False positive: %s"
                                         % cat.title(as_link=True,
                                                     textlink=True))
                    continue
            except pywikibot.exceptions.Error:
                self.log_text.append("* Could not load %s; ignoring"
                                     % cat.title(as_link=True, textlink=True))
                continue
            cat_title = cat.title(with_ns=False)
##            if "requested photographs of" in cat_title or "needing photo" in cat_title:
###                self.log_text.append(u"* Ignoring %s"
###                                     % cat.title(as_link=True, textlink=True))
##                continue
            dest = cat.getCategoryRedirectTarget()
            if not dest.exists():
                problems.append("# %s redirects to %s"
                                % (cat.title(as_link=True, textlink=True),
                                   dest.title(as_link=True, textlink=True)))
                # do a null edit on cat to update any special redirect
                # categories this wiki might maintain
##                try:
##                    if cat.exists():
##                        cat.save()
##                except:
##                    pass
                continue
            if not self.readyToEdit(cat):
                counts[cat_title] = None
                time2go = cat.latest_revision.timestamp + timedelta(days=self.cooldown) - self.today
                days2go = time2go.days
                if time2go.seconds >= 3600:
                    days2go += 1
                self.log_text.append("* Skipping %s; in cooldown period (%d day%s to go)."
                                     % (cat.title(as_link=True, textlink=True),
                                        days2go,
                                        ("s" if days2go != 1 else "")
                                       )
                                    )
                if self.cooldown_msg and not dest.isCategoryRedirect():
                    mems = list(dest.members())
                    if mems in ([], [cat]):
                        # dest category is empty except for the redirect from cat
                        if self.cooldown_msg not in dest.templates():
                            dest.text = self.cooldown_text + dest.text
                            dest.save("Bot: Category is in cooldown period")

                continue
            if self.cooldown_msg in dest.templates():
                dest.text = dest.text.replace(self.cooldown_text, "")
                dest.save("Bot: Cooldown period ended")
            if dest.isCategoryRedirect():
                double = dest.getCategoryRedirectTarget()
                if double == dest or double == cat:
                    self.log_text.append("* Redirect loop from %s"
                                         % dest.title(as_link=True,
                                                      textlink=True))
                    # do a null edit on cat
##                    try:
##                        if cat.exists():
##                            cat.save()
##                    except:
##                        pass
                else:
                    self.log_text.append(
                        "* Fixed double-redirect: %s -> %s -> %s"
                        % (cat.title(as_link=True, textlink=True),
                           dest.title(as_link=True, textlink=True),
                           double.title(as_link=True, textlink=True)))
                    oldtext = cat.text
                    # remove the old redirect from the old text,
                    # leaving behind any non-redirect text
                    oldtext = template_regex.sub("", oldtext)
                    newtext = ("{{%(redirtemp)s|%(ncat)s}}"
                               % {'redirtemp': template_list[0],
                                  'ncat': double.title(with_ns=False)})
                    newtext = newtext + oldtext.strip()
                    try:
                        cat.text = newtext
                        cat.save(i18n.twtranslate(self.site.code,
                                                  self.dbl_redir_comment))
                    except pywikibot.exceptions.Error as e:
                        self.log_text.append("** Failed: %s" % e)
                continue
            found, moved = self.move_contents(cat_title,
                                              dest.title(with_ns=False),
                                              editSummary=comment)
            if found is None:
                self.log_text.append(
                    "* [[:%s%s]]: error in move_contents"
                    % (self.catprefix, cat_title))
            elif found:
                record[cat_title][today] = found
                self.log_text.append(
                    "* [[:%s%s]]: %d found, %d moved"
                    % (self.catprefix, cat_title, found, moved))
            counts[cat_title] = found
            # do a null edit on cat
##            try:
##                if cat.exists():
##                    cat.save()
##            except:
##                pass

        # clean up cooldown templates
        if self.cooldown_msg:
            for dest in self.cooldown_msg.embeddedin():
                try:
                    mems = list(pywikibot.Category(dest).members(total=3))
                    subs = list(pywikibot.Category(dest).subcategories(total=3))
                    if len(mems) > 1 or (len(mems) == 1 and len(subs) == 0) or \
                      (len(subs) == 1 and not subs[0].isCategoryRedirect()):
                        dest.text = dest.text.replace(self.cooldown_text, "")
                        dest.save("Bot: Cooldown message no longer needed")
                except pywikibot.exceptions.Error as err:
                    print(err)

        pickle.dump(record, open(datafile, "wb"), -1)

        self.log_text.sort()
        problems.sort()
        newredirs.sort()
        comment = i18n.twtranslate(self.site.code, self.maint_comment)
        self.log_page.text = ("\n== %i-%02i-%02iT%02i:%02i:%02iZ ==\n"
                              % time.gmtime()[:6]
                              + "\n".join(self.log_text)
                              + "\n* New redirects since last report:\n"
                              + "\n".join(newredirs)
                              + "\n" + "\n".join(problems)
                              + "\n" + self.get_log_text())
        try:
            self.log_page.save(comment)
        except pywikibot.exceptions.Error:
            self.site.logout()
            self.site.login()
            self.log_page.save(comment)
        if self.edit_requests:
            edit_request_page.text = (self.edit_request_text
                                      % {'itemlist': "\n" + "\n".join(
                                          (self.edit_request_item % item)
                                          for item in self.edit_requests)})
            edit_request_page.save(comment)

def main(*args):
    a = pywikibot.handle_args(*args)
    if len(a) == 1:
        raise RuntimeError('Unrecognized argument "%s"' % a[0])
    if a:
        raise RuntimeError('Unrecognized arguments: ' +
                           " ".join(('"%s"' % arg) for arg in a))
    pywikibot.output("")
    bot = CategoryRedirectBot()
    bot.run()

if __name__ == "__main__":
    main()
