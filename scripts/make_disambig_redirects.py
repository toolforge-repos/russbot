#! /usr/bin/python3
"""Create a new "(disambiguation)" redirect to any dab page that needs one."""

import pywikibot
import pywikibot.pagegenerators

import datetime
import gc
import locale
import time
import MySQLdb
from reporttools import *


locale.setlocale(locale.LC_ALL, "")

def fmt(num):
    return locale.format("%i", num, grouping=True)

def datekey(date):
    return date.strftime("%Y-%m-%d")

try:
    offset = 0
    for arg in pywikibot.handle_args():
        try:
            offset = int(arg)
        except:
            pass
    s = pywikibot.Site()
    s.login()
    log = []
    counter = 0
    LIMIT = 6500
    pywikibot.config.put_throttle = 12

    wdb = MySQLdb.connect(db='enwiki_p',
                          host="enwiki.analytics.db.svc.wikimedia.cloud",
                          read_default_file="/data/project/russbot/.my.cnf")

    wcursor = wdb.cursor()
    # get set of all existing (disambiguation) redirects
    wcursor.execute("""
        SELECT /* SLOW_OK */ page1.page_title
        FROM page AS page1
            JOIN redirect ON rd_from=page_id
            JOIN page AS page2
            JOIN categorylinks
        WHERE page2.page_namespace=rd_namespace
          AND page2.page_title=rd_title
          AND cl_from=page2.page_id
          AND cl_to="All_article_disambiguation_pages"
          AND page1.page_title LIKE "%disambiguation%"
        """
    )
    dabredirs = set(pywikibot.Page(s, clean(row[0]))
                    for row in wcursor.fetchall())
    wdb.close()

    udb = MySQLdb.connect(db='s51290__dpl_p',
                          host="tools.labsdb",
                          read_default_file="/data/project/russbot/.my.cnf")
    ucursor = udb.cursor ()
    ucursor.execute("""SELECT DISTINCT /* SLOW_OK */
                       dl_date FROM disambig_links""")
    updates = [row[0] for row in ucursor.fetchall()]
    current = max(updates) # most recent available date

    while True:
        # get list of current disambiguation pages with links, from user db
        ucursor.execute("""\
            SELECT /* SLOW_OK */ dt_title
            FROM disambig_titles
                JOIN disambig_links ON dl_id = dt_id
            WHERE dl_date = DATE('%s') AND left(dt_title, 1)<>"#"
            LIMIT %s, 1000
            """ % (datekey(current), offset)
        )
        dabpages = [pywikibot.Page(s, clean(row[0]))
                    for row in ucursor.fetchall()]
        if not dabpages:
            break
        offset += len(dabpages)

        for dab in pywikibot.pagegenerators.PreloadingGenerator(dabpages):
            try:
                if "(" in dab.title() or ")" in dab.title():
#                    del dab
                    continue
                redir = dab.title() + " (disambiguation)"
                redirpage = pywikibot.Page(s, redir)
                if redirpage in dabredirs or redirpage.exists():
                    # redirect page already exists
#                    del dab, redirpage
                    continue
                if not dab.isDisambig():
#                    del dab
                    continue
                redirpage.text = "#REDIRECT [[%s]]{{R to disambiguation page}}" \
                                  % dab.title()
                try:
                    redirpage.save(
                        summary="Bot: creating redirect to disambiguation page",
                        minor=False)
                    counter += 1
                    if counter >= LIMIT:
                        break
                except pywikibot.Error as e:
                    log.append(
                        "# Unable to create redirect page [[%s]] to [[%s]]: %s"
                        % (redirpage.title(), dab.title(), e))
#                del dab, redirpage
            except pywikibot.exceptions.Error as e:
                log.append(
                    "# Skipped page [[%s]]: %s"
                    % (dab.title(), str(e)))
#                del dab

    print("Last offset: ", offset) 
    if log:
        logpage = pywikibot.Page(s, "User:RussBot/Redirect creation log")
        logpage.put("\n".join(log), "Robot: log report", minorEdit=False)
    udb.close()

except Exception:
    pywikibot.error("Fatal error", exc_info=True)
finally:
    pywikibot.stopme()
