#!/usr/bin/python3
"""Find links that could be extended to disambiguate them."""

import datetime
import MySQLdb
import pywikibot
from pywikibot import pagegenerators
#import locale
import re
from reporttools import clean, quote, write

#locale.setlocale(locale.LC_ALL, "en_US")

pywikibot.handle_args()
s = pywikibot.Site()
s.login()

db = MySQLdb.connect(db='s51290__dpl_p',
                     host="tools.labsdb",
                     charset="utf8",
                     read_default_file="/data/project/russbot/.my.cnf")

cursor = db.cursor()

# generate list of disambiguation pages with incoming links
dab_titles = set()
offset = 0
result = cursor.execute("""\
    SELECT a.dab_title from all_dabs as a
    WHERE (SELECT count(b.article_id) from all_dab_links as b
           WHERE a.dab_id = b.dab_id) > 0;
""")
print("Processing", result, "disambiguation page titles.")
for row in cursor.fetchall():
    try:
        dab_titles.add(clean(row[0]))
    except:
        pass

# generate list of articles that link to disambiguation pages
linking_articles = set()
offset = 0
while True:
    result = cursor.execute("""\
        SELECT DISTINCT article_title from all_dab_links
        LIMIT %(offset)i, 250000;
             """ % locals()
    )
    if not result:
        break
    print("Processing", result, "articles with ambiguous links.")
    offset += result
    for row in cursor.fetchall():
        try:
            linking_articles.add(clean(row[0]))
        except:
            pass

cursor.close()
db.close()

results = []
pattern1 = re.compile(r'\[\[(?P<title>[^\]\|#]*)[^\]]*\]\]([a-z]*)(?P<comma>[, ]+)(?P<following>\w+)',
                      re.U)
pattern2 = re.compile(r'\[\[(?P<title1>[^\]\|#]*)[^\]]*\]\]([a-z]*)(?P<comma>[, ]+)\[\[(?P<title2>[^\]\|#]*)[^\]]*\]\]',
                      re.U)
for p in pagegenerators.PagesFromTitlesGenerator(linking_articles):
    try:
        t = p.text
    except pywikibot.exceptions.ServerError:
        print(f"** Server error reading {p.title}. **")
        continue
    for m1 in pattern1.finditer(t):
        if m1.group("title").strip() in dab_titles:
            new_title = m1.group("title").strip() + m1.group("comma").strip() + " " + m1.group("following").strip()
            new_p = pywikibot.Page(s, new_title)
            try:
                if new_p.exists():
                    while new_p.isRedirectPage():
                        new_p = new_p.getRedirectTarget()
                    if new_p.exists() and new_p != p and not new_p.isDisambig():
                        if "|" in m1.group(0):
                            new_link = m1.group(0).replace("[[", "[[[[").replace("|", "]]|")
                        else:
                            new_link = m1.group(0).replace("[[", "[[[[").replace("]]", "]]]]")
                        results.append(
                            '# [[%s]]: {{mono|"%s"}} could be replaced by {{mono|"[[[[%s]]]]"}}'
                            % (p.title(),
                               new_link,
                               new_title)
                        )
                        if len(results) > 1 and results[-2] == results[-1]:
                            del results[-1]
            except:
                pass
    for m2 in pattern2.finditer(t):
        if m2.group("title1").strip() in dab_titles:
            new_title = m2.group("title1").strip() + m2.group("comma").strip() + " " + m2.group("title2").strip()
            new_p = pywikibot.Page(s, new_title)
            try:
                if new_p.exists():
                    while new_p.isRedirectPage():
                        new_p = new_p.getRedirectTarget()
                    if new_p.exists() and new_p != p and not new_p.isDisambig():
                        if "|" in m2.group(0):
                            new_link = m2.group(0).replace("[[", "[[[[").replace("|", "]]|")
                            rbracket = new_link.rindex(']]')
                            new_link = newlink[:rbracket] + "]]" + newlink[rbracket:]
                        else:
                            new_link = m2.group(0).replace("[[", "[[[[").replace("]]", "]]]]")
                        results.append(
                            '# [[%s]]: {{mono|"%s"}} could be replaced by {{mono|"[[[[%s]]]]"}}'
                            % (p.title(),
                               new_link,
                               new_title)
                        )
                        if len(results) > 1 and results[-2] == results[-1]:
                            del results[-1]
            except:
                pass

write(s, "Wikipedia:WikiProject Disambiguation/Candidates for disambiguation by extending link",
        'Please note: this page is generated daily (usually) by an automated process. Any edits made by other users will be overwritten by the bot at its next run.\n\nLinks to disambiguation pages that might be fixed by extending the link:',
         sorted(results),
         "Bot: creating list from database mirror")
