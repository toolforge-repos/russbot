#! /usr/bin/python

import pywikibot
from collections import defaultdict
import datetime

try:
    s = pywikibot.Site()
    s.login()
    tdablinks = defaultdict(set)
    reportpage = pywikibot.Page(s, "User:RussBot/Templates with links to disambiguation pages")
    linkcount = []
    templatepages = set()
    templatelinks = {}
    all_links = defaultdict(set)
    templategen = pywikibot.data.api.Request(
        action = "query",
        generator = "allpages",
        gapnamespace = 10,
        gapfilterredir = "nonredirects",
        gaplimit = "max",
        prop = "links",
        plnamespace = 0,
        pllimit = "max",
        indexpageids = ""
    )
    print
    print datetime.datetime.now()
    while True:
        response = templategen.submit()
        if "query" not in response or "pageids" not in response["query"]:
            break
        for pid in response["query"]["pageids"]:
            t = response["query"]["pages"][pid]
            if "links" in t:
                templatepages.add(t["title"])
                templatelinks[t["title"]] = set(l["title"] for l in t["links"])
                for l in t["links"]:
                    all_links[l["title"]] = t["title"]
        if "query-continue" in response:
            if "links" in response["query-continue"]:
                templategen.update(response["query-continue"]["links"])
            else:
                if "plcontinue" in templategen:
                    del templategen["plcontinue"]
                templategen.update(response["query-continue"]["allpages"])
        else:
            break
    print
    print datetime.datetime.now()
    print len(templatepages), "template pages with links found."
    print len(all_links), "article links to check."
    list_of_links = all_links.keys()
    disambiglinks = set()
    while list_of_links:
        ttitle = list_of_links[:500]
        list_of_links = list_of_links[500:]
        gen = pywikibot.data.api.Request(
            action = "query",
            titles = ttitle,
            prop = "categories",
            cllimit = "max",
            indexpageids = "",
            redirects = ""
        )
        cresp = gen.submit()
        if "query" not in cresp or "pageids" not in cresp["query"]:
            continue
        query = cresp['query']
        redirectmap = (dict((r["from"], r["to"]) for r in query["redirects"])
                       if "redirects" in query else {})
        # check all links to see which ones point to disambig pages
        for linkid in query["pageids"]:
            linkdata = query["pages"][linkid]
            ltitle = linkdata["title"]
            if linkid < 0:
                # redlink, no need to examine
                continue
            if "categories" in linkdata:
                for c in linkdata["categories"]:
                    if c["title"] == "Category:All article disambiguation pages":
                        break
                else:
                    # ltitle is not a disambiguation page
                    continue
                if not ltitle.endswith(" (disambiguation)"):
                    disambiglinks.add(ltitle)
                # also add all known redirects to ltitle
                for rtitle in redirectmap:
                    if redirectmap[rtitle] == ltitle \
                            and not rtitle.endswith(" (disambiguation)") \
                            and not rtitle.endswith(" (number)"): 
                        disambiglinks.add(rtitle)
    # check all templates to see which ones contain links to disambigs
    for ttitle in templatelinks:
        for ltitle in templatelinks[ttitle]:
            if ltitle in disambiglinks:
                tdablinks[ttitle].add(ltitle)
    print
    print datetime.datetime.now()
    print len(tdablinks), "Templates contain links to dab pages"
    for t in tdablinks:
        if t.endswith("/doc"):
            continue
        if (t + "/doc") in tdablinks:
            for doclink in tdablinks[t + "/doc"]:
                tdablinks[t].discard(doclink)
    for t in tdablinks:
        if t.endswith("/doc"):
            continue
        if tdablinks[t]:
            # filter out noinclude'd links
            # check first four linked pages, and see if any of them
            # have links to the disambiguation page
            testdab = tdablinks[t].pop()
            tdablinks[t].add(testdab)
            embeds = [z for z in pywikibot.Page(s, t).embeddedin(namespaces=0)]
            ok = True
	    testpage = pywikibot.Page(s, testdab)
	    try:
	        for usingpage in embeds[:4]:
		    if testpage not in usingpage.linkedPages():
		        ok = False
		        break
	        if ok:
		    linkcount.append((t, len(embeds)))
	    except:
		pass
    print
    print datetime.datetime.now()
    print len(linkcount), "remain after filtering for doc pages"
    print
    linkcount.sort(key=lambda a: (-a[1], a[0]))

    report = []
    for v in linkcount:
        if v[1] > 1:
            report.append("# [[%s]]: Used in %i articles. Links to disambiguation pages:"
                          % v)
            for l in tdablinks[v[0]]:
                report.append("#*  [[%s]]" % l)
    reportpage.text = "\n".join(report)
    reportpage.save("Robot: generating report")
    
except:
    pywikibot.logging.exception('')
finally:
    pywikibot.stopme()
