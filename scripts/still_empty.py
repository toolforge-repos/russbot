"""Print names of categories empty for four reports"""
import pywikibot
import re
import sys
from bs4 import BeautifulSoup as Soup

def main(*cmdargs):
    global item
    try:
        pywikibot.handleArgs(*cmdargs)
        s = pywikibot.Site()
        s.login()
        source = pywikibot.Page(s,
                    "Wikipedia:Database reports/Empty categories")
        oldrev = source.getVersionHistory(total=29)[-1][0] #usually total=8)
        s.loadrevisions(source, revids=oldrev, getText=True)
        oldtext = source._revisions[oldrev].text
        # get each link from current Empty categories report
        print
        for item in pywikibot.data.api.QueryGenerator(
                site=s,
                generator="links",
                titles=source.title(),
                gplnamespace=14,
                prop="categoryinfo|categories",
                gpllimit=50,
                cllimit="max"):
            # skip false positives
            if item["title"] in ("Category:Wikipedia category redirects",
                                 "Category:Disambiguation categories"):
                continue
            # skip redlinks
            if "missing" in item:
                continue
            # skip non-empty categories
            if "categoryinfo" in item and item["categoryinfo"]["size"] != 0:
                continue
            # skip CFDs and tagged empty cats
            skip = False
            if "categories" in item:
                for cat in item["categories"]:
                    if cat["title"] in (
                            "Category:Categories for deletion",
                            "Category:Candidates for speedy deletion",
                            "Category:Categories for conversion",
                            "Category:Categories for discussion",
                            "Category:Categories for listifying",
                            "Category:Categories for merging",
                            "Category:Categories for renaming",
                            "Category:Categories for speedy renaming",
                            "Category:Categories for splitting",
                            "Category:Empty categories awaiting deletion"):
                        skip = True
                        break
            if skip:
                continue
            # see if this item was in the old list
            t = item["title"].replace("Category:", "").replace(" ", "_")
            m = re.search(r"{{clh\|1=%s}}" % re.escape(t), oldtext)
            if m is not None:
                print item["title"]
    except:
        import traceback
        traceback.print_exc()
    finally:
        pywikibot.stopme()


if __name__ == "__main__":
    main()
