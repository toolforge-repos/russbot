#!/usr/bin/python3
# -*- coding: utf-8  -*-

"""
Report potentially intentional disambiguation links.
"""

import pywikibot
from pywikibot import pagegenerators
from reporttools import write
import re, sys
import MySQLdb

pywikibot.handle_args()
s = pywikibot.Site()
s.login()
results = []

toolsdb = MySQLdb.connect(db='s51290__dpl_p',
                          host='tools.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/russbot/.my.cnf',
                          charset="utf8")
tcursor = toolsdb.cursor(cursorclass=MySQLdb.cursors.DictCursor)

tcursor.execute("""
    SELECT article_title, dab_title
      FROM all_dab_links_basic
     WHERE article_title LIKE '%(%)'
""")

for row in tcursor.fetchall():
    atitle = row['article_title'] #.decode("utf8")
    dtitle = row['dab_title'] #.decode("utf8")
    if atitle.startswith(dtitle):
        results.append(
            "# [[%s]] contains link to [[%s]]" % (atitle, dtitle)
        )

write(s, "Wikipedia:WikiProject Disambiguation/Potentially intentional dablinks",
         "Please note: this page is generated weekly (usually) by an automated process. Any edits made by other users will be overwritten by the bot at its next run.\n\nLinks to disambiguation pages that are unqualified title of linking page:",
         sorted(results),
         "Bot: updating list")

