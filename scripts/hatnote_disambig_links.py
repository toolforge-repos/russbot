#! /usr/bin/python3
"""Articles with links to disambiguation pages near the top."""

import datetime
import MySQLdb
import pywikibot
from pywikibot import pagegenerators
import re
from reporttools import clean, quote, write
from collections import defaultdict

def initcap(title):
    return title[:1].upper() + title[1:]

try:
    disambig = " (disambiguation)"
    pywikibot.handle_args()
    s = pywikibot.Site()
    s.login()
    db = MySQLdb.connect(db='s51290__dpl_p',
                         host="tools.labsdb",
                         read_default_file="/data/project/russbot/.my.cnf",
                         charset="utf8")
    cursor = db.cursor()
    offset = 0
    links = defaultdict(set)
    hits = []

    while True:
        result = cursor.execute("""\
SELECT article_title, redirect_title
/* SLOW_OK */
FROM all_dab_links
WHERE template_id = 0
LIMIT %(offset)i, 5000;"""
                % locals())
        if not result:
            break
        pywikibot.output("Processing %i results." % result)
        offset += result
        for row in cursor.fetchall():
            art = pywikibot.Page(s, clean(row[0]))
            link = pywikibot.Page(s, clean(row[1]))
            links[art].add(link.title())

    cursor.close()
    db.close()

    linkgen = pagegenerators.PreloadingGenerator(
            a for a in links
        )
    for article in linkgen:
        for m in pywikibot.link_regex.finditer(article.text):
            # check all wikilinks starting within the first 100 characters
            if m.start(0) > 100:
                break
            # see if any of them match a known disambig link
            linktitle = initcap(m.group('title'))
            if linktitle in links[article]:
                hits.append(
    "# [[%s]] contains link to [[%s]] at position %s"
                    % (article.title(), linktitle, m.start(0))
                )
                if len(hits) % 1000 == 0:
                    pywikibot.output("%i hits" % len(hits))

    write(s, "Wikipedia:WikiProject Disambiguation/Possible hatnote disambiguation links",
             'Please note: this page is generated daily (usually) by an automated process. Any edits made by other users will be overwritten by the bot at its next run.\n\nArticles that have disambiguation links in the first 100 characters:',
             sorted(hits),
             "Bot: creating list from database mirror",
             chunksize=1000,
          )

except Exception:
    pywikibot.error("Error", exc_info=True)
finally:
    pywikibot.stopme()
