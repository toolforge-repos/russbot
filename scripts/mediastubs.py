import pywikibot
s = pywikibot.Site(user="R'n'B")
s.login()

nonempty = pywikibot.Category(s, "Category:Wikipedia non-empty soft redirected categories")
for cat in nonempty.subcategories():
    if not cat.title().endswith(" media"):
        continue
    stubcat = pywikibot.Category(s, cat.title() + " stubs")
    if not stubcat.exists():
        continue
    country = pywikibot.input(u"Change '%s' to 'Media in __'?" % cat.title(withNamespace=False))
    stubcat.text = stubcat.text.replace(cat.title(withNamespace=False), "Media in " + country)
    stubcat.save(minor=True, summary=u"Parent category was renamed")

