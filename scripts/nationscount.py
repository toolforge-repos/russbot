#!/usr/bin/python3

import pywikibot
from pywikibot import pagegenerators

article_list = (
#    'America',
    'American',
#    'American people',
    'Britain',
    'British',
    'Chinese',
    'Dutch',
    'English',
    'French',
    'Galicia',
    'Georgia',
    'German',
    'Greek',
#    'Italia',
    'Indian',
    'Italian',
    'Japanese',
    'New York',
    'Portuguese',
    'Roman',
    'Russian',
    'Spanish',
    'Turk',
    'Turkish',
    'Washington',
    'Broadway',
    'Rock'
)


from refcount import mainrefs

if __name__ == "__main__":
    try:
        args = pywikibot.handle_args()
        pywikibot.config.maxlag = 0
        site = pywikibot.Site()
        site.login()
        try:
            for title in article_list:
                page = pywikibot.Page(site, title)
                pywikibot.output("%s: %i main-space links"
                                 % (page.title(),
                                    sum(1 for ref in mainrefs(page))))

        except Exception as e:
            pywikibot.error("Fatal error:", exc_info=True)

    finally:
        pywikibot.stopme()
        done = input("Press Enter when done")
