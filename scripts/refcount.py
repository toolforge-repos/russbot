#! /usr/bin/python
"""Print number of main-space links referring to page(s) named on command line.

Excludes redirects and links to 'Original page (disambiguation)'"""

import sys
import pywikibot


def mainrefs(page, namespaces=0):
    site = pywikibot.Site()
    if isinstance(namespaces, str):
        namespaces = str(namespaces)
    else:
        try:
            namespaces = "|".join(str(i) for i in namespaces)
        except TypeError:
            namespaces = str(namespaces)

    for refpage in page.backlinks(follow_redirects=False, namespaces=[0]):
        if refpage.isRedirectPage():
            if " (disambiguation)" in refpage.title():
                continue
            for ref in mainrefs(refpage):
                yield ref
        else:
            yield refpage


def main(*cmdargs):
    try:
        detail = False
        args = [] 
        for arg in pywikibot.handle_args(*cmdargs):
            if arg == "-detail":
                detail = True
            else:
                args.append(arg)
        site = pywikibot.Site()
        for arg in args:
            try:
                page = pywikibot.Page(site, arg)
                refs = list(mainrefs(page))
                print("%s: %i main-space links" % (arg, len(refs)))
                if detail:
                    for ref in refs:
                        print("  " + ref.title())
            except Exception as e:
                print("%s: Exception raised:\n%s" % (arg, e))
    except:
        import traceback
        traceback.print_exc()
    finally:
        pywikibot.stopme()


if __name__ == "__main__":
    main()
