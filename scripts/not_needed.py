#!/usr/bin/python
# -*- coding: utf-8  -*-

"""
Report incorrect use of 'disambiguation needed' template.
"""

import MySQLdb
import pywikibot
from pywikibot import pagegenerators
from reporttools import clean, quote, write
import re, sys


pywikibot.handle_args()
s = pywikibot.Site()
s.login()
results = []

db = MySQLdb.connect(db='enwiki_p',
                     host="enwiki.analytics.db.svc.wikimedia.cloud",
                     read_default_file="/data/project/russbot/.my.cnf")

cursor = db.cursor()
while True:
    count = cursor.execute("""\
        SELECT p1.page_title FROM page AS p1
        JOIN templatelinks ON p1.page_id=tl_from
        JOIN linktarget AS link1 ON link1.lt_id = tl_target_id
        WHERE p1.page_namespace=0
        AND link1.lt_namespace=10
        AND link1.lt_title="Disambiguation_needed"
        AND NOT EXISTS (SELECT * FROM pagelinks AS l1
             JOIN linktarget AS t1 ON l1.pl_target_id = t1.lt_id
             JOIN page AS p2 ON p2.page_title=t1.lt_title
             JOIN page_props AS d1 ON d1.pp_page = p2.page_id
             WHERE p2.page_namespace = 0
             AND t1.lt_namespace = 0
             AND l1.pl_from = p1.page_id
             AND d1.pp_propname = "disambiguation")
        AND NOT EXISTS (SELECT * FROM pagelinks AS l2
             JOIN linktarget AS t2 ON l2.pl_target_id = t2.lt_id
             JOIN page AS p3 ON p3.page_title=t2.lt_title
             JOIN redirect ON rd_from = p3.page_id
             JOIN page AS p4 ON p4.page_title = rd_title
             JOIN page_props AS d2 ON d2.pp_page = p4.page_id
             WHERE p3.page_namespace = 0
             AND p3.page_is_redirect
             AND rd_namespace=0
             AND p4.page_namespace=0
             AND l2.pl_from = p1.page_id
             AND d2.pp_propname = "disambiguation");
    """)
    break
for row in cursor.fetchall():
    results.append("# [[%s]]" % clean(row[0]))


write(s, "User:RussBot/Disambiguation not needed",
         "Articles containing {{tl|dn}} without linking to disambiguation pages:",
         sorted(results),
         "Bot: updating list")

