#! /usr/bin/python3
# fix disambig links identified on report
import pywikibot
import re

s = pywikibot.Site(user="R'n'B")
#s.login()
pattern = re.compile(
    r'# \[\[(?P<page_to_edit>[^\]]+)\]\]: \{\{mono\|"(?P<oldtext>[^"]+)"\}\} could be replaced by \{\{mono\|"\[\[\[\[(?P<newlink>[^\]]+)\]\]\]\]"\}\}'
    )
dabpattern = re.compile(r'\[\[([^\]\|#]+)\]\]')
report = pywikibot.Page(s, "Wikipedia:WikiProject Disambiguation/Candidates for disambiguation by extending link").text
lastpage = None
for line in reversed(report.splitlines()):
    m = pattern.match(line)
    if not m:
        continue
    oldtext = m.group("oldtext").replace("[[[[", "[[").replace("]]]]", "]]")
    n = dabpattern.search(oldtext) # extract dabpage link from text
    if not n:
        print(("No wikilink found in '%s'" % oldtext))
        continue
    dablink = n.group(1)
    newtext = "[[" + m.group("newlink") + "]]"
    page = pywikibot.Page(s, m.group("page_to_edit"))
    oldpagetext = page.text
    newpagetext = oldpagetext.replace(oldtext, newtext)
    if newpagetext == oldpagetext:
        try:
            print(("Not found in [[%s]]" % page.title()))
        except:
            pass
        continue
    pywikibot.output("\n\n>>> \03{lightpurple}%s\03{default} <<<"
                     % page.title())
    if page == lastpage:
        pywikibot.output(">>> \03{red}Warning: repeated page\03{default} <<<")
    lastpage = page
    pywikibot.showDiff(oldpagetext, newpagetext)
    choice = pywikibot.input_choice(
        'Do you want to accept these changes?',
        [('Yes', 'y'), ('No', 'n'), ('Bypass redirect', 'b')],
        default='N')
    if choice == 'y':
        try:
            page.text = newpagetext
            page.save(summary="Disambiguate link to [[%s]]" % dablink, asynchronous=True)
        except:
            pywikibot.exception()
    elif choice == 'b':
        newpage = pywikibot.Page(s, m.group("newlink"))
        if not newpage.isRedirectPage():
            pywikibot.output("ERROR: not a redirect")
        else:
            target = newpage.getRedirectTarget()
            newtext = target.title(as_link = True)
            newpagetext = oldpagetext.replace(oldtext, newtext)
            pywikibot.showDiff(oldpagetext, newpagetext)
            try:
                page.text = newpagetext
                page.save(summary="Disambiguate link to [[%s]]" % dablink, asynchronous=True)
            except:
                pywikibot.exception()
    # choice must be 'N'


